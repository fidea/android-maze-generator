# Android Maze Generator

Generate and display mazes. Library provides tools to generate and display views in Android application

## Use the library

All the information about how is this library published is [here](https://bintray.com/anton-wiedermann/maven/eu.espoar.mazeview).

### Add the dependency

Library is available via jcenter so make sure you have it included as a repository
```
#!gradle
    repositories {
        jcenter()
    }
```

To add library as a compile dependency add following to your dependencies
```
#!gradle
    compile 'eu.espoar.mazeview:maze-view:1.0.2@aar'
```

### Add MazeView to layout

Once you have added the dependency you can add the view to your application layout

```
#!xml
    <eu.espoar.mazeview.MazeView
        android:id="@+id/maze"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
```

### Initialize the maze

Change Activity#onCreate to create the maze

```
#!java
    /** reference to the maze view */
    private MazeView mazeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get reference to the maze view
        mazeView = (MazeView)findViewById(R.id.maze);
        // generate new maze with desired properties
        mazeView.generateMaze(15, 10, 0.5d);
    }
```
 
## Non-android use

Library is perfectly fine for use in any java application but I'll have to tweak build scripts to produce plain jar file

## Credits

Fork of Java8-heavy implementation of generator which I had to adapt for use in Android application.