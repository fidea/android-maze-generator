/**
 * 
 */
package eu.espoar.mazeview.mazegen.generation;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.espoar.mazeview.mazegen.generation.Direction;
import eu.espoar.mazeview.mazegen.generation.Position;

/**
 * @author tono
 */
public class PositionTest {

	@Test
	public void testConstruction() {
		// make sure that the position constructor stores values as expected
		int row = 3;
		int col = 5;
		checkPosition(new Position(row, col), row, col);
	}

	@Test
	public void testMove() {
		// initial position
		Position start = new Position(3, 4);
		// make the moves and check results
		checkPosition(start.move(Direction.UP, 1), 2, 4);
		checkPosition(start.move(Direction.RIGHT, 1), 3, 5);
		checkPosition(start.move(Direction.UP, 3), 0, 4);
		checkPosition(start.move(Direction.UP, 5), -2, 4);
		checkPosition(start.move(Direction.LEFT, 1), 3, 3);
		checkPosition(start.move(Direction.LEFT, 4), 3, 0);
		checkPosition(start.move(Direction.LEFT, 6), 3, -2);
	}
	
	/**
	 * test that the position is what is is expected to be
	 * 
	 * @param pos position to test
	 * @param row expected row
	 * @param col expected column
	 */
	public static void checkPosition(Position pos, int row, int col) {
		assertEquals("checking row", row, pos.row);
		assertEquals("checking column", col, pos.column);
	}
}
