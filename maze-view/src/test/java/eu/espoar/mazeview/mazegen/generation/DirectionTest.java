/**
 * 
 */
package eu.espoar.mazeview.mazegen.generation;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author tono
 */
public class DirectionTest {

	@Test
	public void testItems() {
		// test that directions have expected offsets
		testDirectionOffsets(Direction.DOWN, 1, 0);
		testDirectionOffsets(Direction.UP, -1, 0);
		testDirectionOffsets(Direction.LEFT, 0, -1);
		testDirectionOffsets(Direction.RIGHT, 0, 1);
	}

	@Test
	public void testApplyToPosition() {
		// check that direction can move the position as expected
		Position start = new Position(3, 4);
		PositionTest.checkPosition(Direction.DOWN.applyTo(start, 1), 4, 4);
		PositionTest.checkPosition(Direction.DOWN.applyTo(start, 2), 5, 4);
		PositionTest.checkPosition(Direction.UP.applyTo(start, 4), -1, 4);
		PositionTest.checkPosition(Direction.LEFT.applyTo(start, 1), 3, 3);
		PositionTest.checkPosition(Direction.LEFT.applyTo(start, 5), 3, -1);
	}
	
	public static void testDirectionOffsets(Direction direction, int expectedRowOffset, int expectedColOffset) {
		assertEquals("check row offset", expectedRowOffset, direction.getRowOffset());
		assertEquals("check col offset", expectedColOffset, direction.getColumnOffset());
	}
}
