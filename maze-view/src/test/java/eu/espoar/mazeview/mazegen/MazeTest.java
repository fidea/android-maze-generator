/**
 * 
 */
package eu.espoar.mazeview.mazegen;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import eu.espoar.mazeview.mazegen.Maze;
import eu.espoar.mazeview.mazegen.generation.Position;

/**
 * @author tono
 */
public class MazeTest {

    @Rule
    public ExpectedException thrown= ExpectedException.none();
    
	@Test
	public void testConstructorCheck() {
		// test row counts
		try {
			new Maze(-1, 7);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(0, 7);
			fail("Should have failed");	
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(1, 7);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(2, 7);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		new Maze(3, 7);
		// test column counts
		try {
			new Maze(7, -1);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(7, 0);
			fail("Should have failed");	
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(7, 1);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		try {
			new Maze(7, 2);
			fail("Should have failed");
		} catch (IllegalArgumentException e) {}
		new Maze(7, 3);
	}   
	
	@Test
	public void testConstructor() {
		Maze maze = new Maze(6, 7);
		assertEquals("check row count", 6, maze.rows());
		assertEquals("check col count", 7, maze.columns());
	}

	@Test
	public void testInitialData() {
		Maze maze = new Maze(6, 7);
		assertEquals("checking initial walls", Maze.WALL, maze.at(0, 0));
		assertEquals("checking initial walls", Maze.WALL, maze.at(0, 1));
		assertEquals("checking initial walls", Maze.WALL, maze.at(0, 2));
		assertEquals("checking initial walls", Maze.WALL, maze.at(1, 0));
		assertEquals("checking initial walls", Maze.WALL, maze.at(2, 0));
		assertEquals("checking initial walls", Maze.WALL, maze.at(5, 6));
	}

	@Test
	public void testBorders() {
		Maze maze = new Maze(6, 7);
		assertEquals("checking borders", Maze.WALL, maze.at(0, 0));
		assertEquals("checking borders", Maze.OUTSIDE, maze.at(0, -1));
		assertEquals("checking borders", Maze.OUTSIDE, maze.at(-1, 0));
		assertEquals("checking borders", Maze.WALL, maze.at(5, 6));
		assertEquals("checking borders", Maze.OUTSIDE, maze.at(6, 6));
		assertEquals("checking borders", Maze.OUTSIDE, maze.at(5, 7));
	}

	@Test
	public void testModification() {
		Maze maze = new Maze(6, 7);
		assertEquals("checking for wall", Maze.WALL, maze.at(2, 3));
		maze.setAisle(new Position(2,3));
		assertEquals("checking for aisle", Maze.AISLE, maze.at(2, 3));
	}

	@Test
	public void testFailWriteToOutside() {
		Maze maze = new Maze(6, 7);
		// expect exception out of this one
		thrown.expect(ArrayIndexOutOfBoundsException.class);
		// write to the outside
		maze.setAisle(new Position(7,7));
	}
}
