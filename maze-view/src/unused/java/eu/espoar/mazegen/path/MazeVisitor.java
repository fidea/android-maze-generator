package eu.espoar.mazegen.path;

import eu.espoar.mazegen.grid.Grid;

public interface MazeVisitor {

    Grid<Integer> advance(Grid<Integer> maze);

    default boolean isEnded() {
        return false;
    }
}
