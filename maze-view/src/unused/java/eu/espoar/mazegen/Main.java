package eu.espoar.mazegen;

import java.util.function.Function;

import eu.espoar.mazegen.grid.ArrayGrid;
import eu.espoar.mazegen.grid.Grid;
import eu.espoar.mazegen.grid.Grid.Location;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
    	// initial size of the grid
        final int size = 80;
        // create new grid of specified initial size
        final Grid<Integer> maze = ArrayGrid.of(size, size);
        maze.apply(new InitialMapper());
        final Scene scene = new Scene(new Mazer(maze, 640 / size));
        stage.setScene(scene);
        stage.setTitle("Mazer");
        stage.show();
    }
    
    /**
     * Initial mapper which draws pattern before we actually start
     * 
     * @author tono
     */
    private static class InitialMapper implements Mapper<Integer> {
		@Override
		public Integer map(Location<Integer> location) {
			return location.row() >= 35 && location.row() < 45 && location.column() >= 35 && location.column() < 45 ? Maze.AISLE : Maze.WALL;
		}
    	
    }
}
