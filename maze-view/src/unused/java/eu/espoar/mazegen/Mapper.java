/**
 * 
 */
package eu.espoar.mazegen;

import eu.espoar.mazegen.grid.Grid.Location;

/**
 * @author tono
 *
 */
public interface Mapper<E> {

	/**
	 * map the location to the element
	 * 
	 * @param location
	 * @return
	 */
	E map(Location<E> location);
}
