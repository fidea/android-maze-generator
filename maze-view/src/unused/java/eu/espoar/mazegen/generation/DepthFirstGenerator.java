package eu.espoar.mazegen.generation;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import eu.espoar.mazegen.Pair;
import eu.espoar.mazegen.grid.Grid;

/**
 * Maze generator built around depth first search with somewhat random direction
 * 
 * Expects maze to be pre-initialized to all-walls
 * 
 * @author tono
 */
public class DepthFirstGenerator extends MazeGenerator {

    private final Deque<Pair<Integer, Integer>> path = new LinkedList<>();
    private Direction direction;
    private final Random random;

    /**
     * initialize new generator
     * 
     * @param initialRow
     * @param direction
     * @param random
     */
    public DepthFirstGenerator(int initialRow, int initialColumn, Random random) {
    	// put initial position on the path
        path.push(new Pair<>(initialRow, initialColumn));
        // store reference to random
        this.random = random;
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
    	// if path is empty then maze is considered done
        if (path.isEmpty()) {
            return maze;
        }
        // set first item on the path as aisle
        final Pair<Integer, Integer> cell = path.peek();
        maze.at(cell.first, cell.second).set(MazeGenerator.AISLE);
        // get list of possible directions
        final List<Direction> neighbours = nonVisitedNeighbours(maze, cell.first, cell.second);
        // if there is no place to move remove current path item and recursively move on
        if (neighbours.isEmpty()) {
            path.remove();
            return advance(maze);
        }
        // pick direction to follow
        chooseDirection(neighbours);
        // set aisle at the picked direction
        maze.at(cell.first + direction.rowOffset(), cell.second + direction.columnOffset()).set(MazeGenerator.AISLE);
        
        path.push(new Pair<>(cell.first + direction.rowOffset() * 2, cell.second + direction.columnOffset() * 2));
        return maze;
    }

    @Override
    public boolean isEnded() {
        return path.isEmpty();
    }

    /**
     * get list of available directions
     * 
     * @param maze
     * @param row
     * @param column
     * @return
     */
    private List<Direction> nonVisitedNeighbours(Grid<Integer> maze, int row, int column) {
        final List<Direction> neighbours = new ArrayList<>();
        // can we move up?
        if (row >= 2 && maze.at(row - 2, column).value() == MazeGenerator.WALL) {
            neighbours.add(Direction.UP);
        }
        // can we move down?
        if (row < maze.rows() - 2 && maze.at(row + 2, column).value() == MazeGenerator.WALL) {
            neighbours.add(Direction.DOWN);
        }
        // can we move left?
        if (column >= 2 && maze.at(row, column - 2).value() == MazeGenerator.WALL) {
            neighbours.add(Direction.LEFT);
        }
        // can we move right?
        if (column < maze.columns() - 2 && maze.at(row, column + 2).value() == MazeGenerator.WALL) {
            neighbours.add(Direction.RIGHT);
        }
        return neighbours;
    }

    /**
     * choose direction to move in. If possible then in 75% of cases do not even try to change the direction
     * 
     * @param availableDirections
     */
    private void chooseDirection(List<Direction> availableDirections) {
    	// change direction only if is not available anymore or 25% just happened
        if (!availableDirections.contains(direction) || random.nextDouble() >= 0.75) {
        	// pick random direction from available directions
            direction = availableDirections.get(random.nextInt(availableDirections.size()));
        }
    }
}
