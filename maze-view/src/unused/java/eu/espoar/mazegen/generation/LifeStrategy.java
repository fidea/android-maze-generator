package eu.espoar.mazegen.generation;

import java.util.function.IntPredicate;

import eu.espoar.mazegen.Maze;
import eu.espoar.mazegen.grid.Grid;

public class LifeStrategy<E> implements MazeGenerator {

    private final IntPredicate birth;
    private final IntPredicate survival;

    public LifeStrategy(IntPredicate birth, IntPredicate survival) {
        this.birth = birth;
        this.survival = survival;
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
        return maze.apply(this::apply);
    }

    private int apply(Grid.Location<Integer> location) {
        final int aliveNeighbours = aliveNeighbours(location);
        if (!survival.test(aliveNeighbours)) {
            return Maze.WALL;
        } else if (birth.test(aliveNeighbours)) {
            return Maze.AISLE;
        } else {
            return location.value();
        }
    }

    private int aliveNeighbours(Grid.Location<Integer> location) {
        return countAlive(location.moveBy(-1, -1))
                + countAlive(location.moveBy(-1, 0))
                + countAlive(location.moveBy(-1, 1))
                + countAlive(location.moveBy(0, -1))
                + countAlive(location.moveBy(0, 1))
                + countAlive(location.moveBy(1, -1))
                + countAlive(location.moveBy(1, 0))
                + countAlive(location.moveBy(1, 1));
    }

    private int countAlive(Grid.Location<Integer> cell) {
    	if (cell != null) {
    		return cell.value() == Maze.AISLE ? 1 : 0;
    	}
    	return 0;
    }
}
