package eu.espoar.mazegen.generation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import eu.espoar.mazegen.Maze;
import eu.espoar.mazegen.grid.Grid;
import javafx.util.Pair;

public class KruskalGenerator implements MazeGenerator {

    private final Map<Pair<Integer, Integer>, Set<Pair<Integer, Integer>>> cells;
    private final List<Wall> walls;
    private final Random random;

    public KruskalGenerator(Random random) {
        this.random = random;
        cells = new HashMap<>();
        walls = new LinkedList<>();
    }

    @Override
    public Grid<Integer> advance(Grid<Integer> maze) {
        if (cells.isEmpty()) {
            for (int r = 0; r < maze.rows(); r += 2) {
                for (int c = 0; c < maze.columns(); c += 2) {
                    final Pair<Integer, Integer> cell = new Pair<>(r, c);
                    cells.put(cell, new HashSet<>(Arrays.asList(cell)));
                    walls.add(new Wall(r, c, r + 2, c));
                    walls.add(new Wall(r, c, r, c + 2));
                }
            }
        }
        if (walls.isEmpty()) {
            return maze;
        }
        final Wall wall = walls.remove(random.nextInt(walls.size()));
        if (wall.endRow >= maze.rows() || wall.endColumn >= maze.columns()) {
            return maze;
        }
        final Set<Pair<Integer, Integer>> formerSet = cells.get(wall.start());
        final Set<Pair<Integer, Integer>> latterSet = cells.get(wall.end());
        if (formerSet.contains(wall.end()) || latterSet.contains(wall.start())) {
            return maze;
        }
        new ArrayList<>(formerSet).forEach(cell -> cells.get(cell).addAll(latterSet));
        new ArrayList<>(latterSet).forEach(cell -> cells.get(cell).addAll(formerSet));
        return maze.apply(location -> {
            if (location.row() == wall.startRow && location.column() == wall.startColumn) {
                return Maze.AISLE;
            }
            if (location.row() == wall.endRow && location.column() == wall.endColumn) {
                return Maze.AISLE;
            }
            if (location.row() == (wall.startRow + wall.endRow) / 2 && location.column() == (wall.startColumn + wall.endColumn) / 2) {
                return Maze.AISLE;
            }
            return location.value();
        });
    }

    @Override
    public boolean isEnded() {
        return walls.isEmpty() && !cells.isEmpty();
    }

    private static class Wall {

        public final int startRow;
        public final int startColumn;
        public final int endRow;
        public final int endColumn;

        public Wall(int startRow, int startColumn, int endRow, int endColumn) {
            this.startRow = startRow;
            this.startColumn = startColumn;
            this.endRow = endRow;
            this.endColumn = endColumn;
        }

        public Pair<Integer, Integer> start() {
            return new Pair<>(startRow, startColumn);
        }

        public Pair<Integer, Integer> end() {
            return new Pair<>(endRow, endColumn);
        }
    }
}
