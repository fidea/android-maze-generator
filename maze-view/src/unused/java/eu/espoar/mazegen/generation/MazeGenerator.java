package eu.espoar.mazegen.generation;

import eu.espoar.mazegen.grid.Grid;

public abstract class MazeGenerator {

    public static final int WALL = 0;
    public static final int AISLE = 0xFFFFFF;
    
    public abstract Grid<Integer> advance(Grid<Integer> maze);

    public boolean isEnded() {
        return false;
    }
}
