package eu.espoar.mazegen;

import java.io.IOException;
import java.nio.file.Path;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;

import eu.espoar.mazegen.grid.ArrayGrid;
import eu.espoar.mazegen.grid.Grid;

public class MazeView {

    private MazeView() {
    }

    public static Color colorize(int cell) {
        return Color.rgb((cell >> 16) & 0xFF, (cell >> 8) & 0xFF, cell & 0xFF);
    }

    public static int cellByColor(Color color) {
        return (((int) (color.getRed() * 255)) << 16) + (((int) (color.getGreen() * 255)) << 8) + ((int) (color.getBlue() * 255));
    }

    public static void persist(Grid<Integer> maze, Path target) {
        final WritableImage image = new WritableImage(maze.columns(), maze.rows());
        final PixelWriter writer = image.getPixelWriter();
        maze.locations().forEach(cell -> writer.setColor(cell.column(), cell.row(), colorize(cell.value())));
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", target.toFile());
        } catch (IOException exception) {
            throw new IllegalStateException(exception.getMessage(), exception);
        }
    }

    public static Grid<Integer> load(Path source) {
        try {
            final WritableImage image = SwingFXUtils.toFXImage(ImageIO.read(source.toFile()), null);
            final PixelReader reader = image.getPixelReader();
            final Grid<Integer> maze = ArrayGrid.of((int) image.getHeight(), (int) image.getWidth());
            return maze.apply(location -> cellByColor(reader.getColor(location.column(), location.row())));
        } catch (IOException exception) {
            throw new IllegalStateException(exception.getMessage(), exception);
        }
    }
}
