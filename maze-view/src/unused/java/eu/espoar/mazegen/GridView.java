package eu.espoar.mazegen;

import java.util.function.Function;

import eu.espoar.mazegen.grid.Grid;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class GridView<E> extends Canvas {

    private final Property<Grid<E>> maze;
    private final Function<? super E, ? extends Color> elementColor;
    private final DoubleProperty rate;

    public GridView(Property<Grid<E>> maze, Function<? super E, ? extends Color> elementColor, DoubleProperty rate) {
        super(maze.getValue().columns() * rate.get(), maze.getValue().rows() * rate.get());
        this.maze = maze;
        this.elementColor = elementColor;
        this.rate = rate;
        widthProperty().bind(Bindings.createDoubleBinding(this::width, maze, rate));
        heightProperty().bind(Bindings.createDoubleBinding(this::height, maze, rate));
    }

    private double width() {
        return rate.get() * maze.getValue().columns();
    }

    private double height() {
        return rate.get() * maze.getValue().rows();
    }

    public void refresh() {
        final double cellSize = rate.get();
        final GraphicsContext graphics = this.getGraphicsContext2D();
        graphics.save();
        maze.getValue().locations().forEach(location -> {
            graphics.setFill(elementColor.apply(location.value()));
            graphics.fillRect(location.column() * cellSize, location.row() * cellSize, cellSize, cellSize);
        });
        graphics.restore();
    }
}
