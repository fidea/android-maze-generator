package eu.espoar.mazegen;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

import eu.espoar.mazegen.generation.DepthFirstGenerator;
import eu.espoar.mazegen.generation.KruskalGenerator;
import eu.espoar.mazegen.generation.LifeStrategy;
import eu.espoar.mazegen.generation.MazeGenerator;
import eu.espoar.mazegen.generation.PrimGenerator;
import eu.espoar.mazegen.grid.ArrayGrid;
import eu.espoar.mazegen.grid.Grid;
import eu.espoar.mazegen.path.BFS;
import eu.espoar.mazegen.path.MazeVisitor;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

public class Mazer extends BorderPane {

    private final Property<Grid<Integer>> maze;
    private final GridView<Integer> view;
    private final Slider sizeRate;
    private final BooleanProperty running;
    private final Property<MazeGenerator> generator;
    private final Property<MazeVisitor> visitor;
    private final TextField rows;
    private final TextField columns;
    private final ComboBox<String> strategies;
    private final ComboBox<String> visitors;
    private final CheckBox interactiveGeneration;
    private final CheckBox interactiveVisit;

    public Mazer(Grid<Integer> maze, double rate) {
        this.maze = new SimpleObjectProperty<>(maze);
        this.sizeRate = new Slider(1, rate, rate);
        view = new GridView<>(this.maze, Maze::colorize, this.sizeRate.valueProperty());
        running = new SimpleBooleanProperty(false);
        rows = new TextField(String.valueOf(maze.rows()));
        columns = new TextField(String.valueOf(maze.columns()));
        strategies = new ComboBox<>(FXCollections.observableArrayList(GENERATION_STRATEGIES.keySet()));
        visitors = new ComboBox<>(FXCollections.observableArrayList(VISITORS.keySet()));
        generator = new SimpleObjectProperty<>();
        visitor = new SimpleObjectProperty<>();
        interactiveGeneration = new CheckBox("Interactive");
        interactiveVisit = new CheckBox("Interactive");
        initComponents();
    }

    private void initComponents() {
        setPadding(new Insets(10));
        setBackground(new Background(new BackgroundFill(Color.DARKSLATEGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        setLeft(new VBox(10,
                mazePane(),
                generationPane(),
                pathPane(),
                sizePane(),
                persistencePane()
        ));
        setMargin(getLeft(), new Insets(0, 10, 0, 0));
        setCenter(view);
        view.setOnMouseDragged(this::setCellValue);
        view.setOnMousePressed(this::setCellValue);
        maze.addListener(mazeChanged -> {
            rows.setText(String.valueOf(maze.getValue().rows()));
            columns.setText(String.valueOf(maze.getValue().columns()));
            view.refresh();
        });
        strategies.valueProperty().addListener(change -> updateGeneratorStrategy());
        strategies.setValue(strategies.getItems().get(0));
        visitors.valueProperty().addListener(change -> updateVisitor());
        visitors.setValue(visitors.getItems().get(0));
        sizeRate.valueProperty().addListener(event -> view.refresh());
        view.refresh();
    }

    private void updateGeneratorStrategy() {
        generator.setValue(GENERATION_STRATEGIES.get(strategies.getValue()).get());
    }

    private void updateVisitor() {
        visitor.setValue(VISITORS.get(visitors.getValue()).get());
    }

    private TitledPane mazePane() {
        rows.setPrefColumnCount(3);
        columns.setPrefColumnCount(3);
        return new TitledPane("maze",
                new VBox(4,
                        new HBox(4,
                                new Label("rows:"),
                                rows,
                                new Label("columns:"),
                                columns
                        ) {
                            {
                                setAlignment(Pos.CENTER);
                            }
                        },
                        new HBox(4,
                                new Button("create") {
                                    {
                                        final BooleanBinding valid = Bindings.createBooleanBinding(() -> isValidNumber(rows.getText()) && isValidNumber(columns.getText()), rows.textProperty(), columns.textProperty());
                                        disableProperty().bind(valid.not());
                                        setOnAction(event -> {
                                            final Grid<Integer> created = ArrayGrid.of(Integer.parseInt(rows.getText()), Integer.parseInt(columns.getText()));
                                            created.fill(Maze.WALL);
                                            maze.setValue(created);
                                        });
                                    }
                                })));
    }

    private static boolean isValidNumber(String value) {
        try {
            return Integer.parseInt(value) >= 0;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    private TitledPane generationPane() {
        return new TitledPane("generation",
                new VBox(4,
                        new HBox(4,
                                new Label("strategy:"),
                                strategies) {
                            {
                                setAlignment(Pos.CENTER_LEFT);
                            }
                        },
                        interactiveGeneration,
                        new HBox(4,
                                new Button("start") {
                                    {
                                        disableProperty().bind(running.or(strategies.valueProperty().isNull()));
                                        setOnAction(event -> {
                                            if (running.get()) {
                                                return;
                                            }
                                            running.set(true);
                                            runGenerator();
                                        });
                                    }
                                }, new Button("stop") {
                            {
                                disableProperty().bind(running.not());
                                setOnAction(event -> running.set(false));
                            }
                        }, new Button("reset") {
                            {
                                disableProperty().bind(running);
                                setOnAction(event -> {
                                    maze.getValue().fill(Maze.WALL);
                                    view.refresh();
                                    updateGeneratorStrategy();
                                });
                            }
                        })));
    }

    private TitledPane pathPane() {
        return new TitledPane("path",
                new VBox(4,
                        new HBox(4,
                                new Label("visitor:"),
                                visitors) {
                            {
                                setAlignment(Pos.CENTER_LEFT);
                            }
                        },
                        interactiveVisit,
                        new HBox(4,
                                new Button("start") {
                                    {
                                        disableProperty().bind(running.or(visitors.valueProperty().isNull()));
                                        setOnAction(event -> {
                                            if (running.get()) {
                                                return;
                                            }
                                            running.set(true);
                                            runVisitor();
                                        });
                                    }
                                }, new Button("stop") {
                            {
                                disableProperty().bind(running.not());
                                setOnAction(event -> running.set(false));
                            }
                        }, new Button("reset") {
                            {
                                disableProperty().bind(running);
                                setOnAction(event -> {
                                    maze.getValue().apply(location -> location.value() == Maze.WALL ? Maze.WALL : Maze.AISLE);
                                    view.refresh();
                                    updateVisitor();
                                });
                            }
                        })));
    }

    public TitledPane sizePane() {
        sizeRate.setShowTickMarks(true);
        sizeRate.setShowTickLabels(true);
        return new TitledPane("Size rate",
                new HBox(4, sizeRate));
    }

    public TitledPane persistencePane() {
        return new TitledPane("file",
                new HBox(4,
                        new Button("load") {
                            {
                                setOnAction(Mazer.this::showLoadDialog);
                            }
                        }, new Button("save") {
                    {
                        setOnAction(Mazer.this::showSaveDialog);
                    }
                }));
    }

    private void runGenerator() {
        if (!running.get()) {
            view.refresh();
            return;
        }
        if (generator.getValue().isEnded()) {
            running.set(false);
            updateGeneratorStrategy();
            view.refresh();
            return;
        }
        Platform.runLater(() -> {
            maze.setValue(generator.getValue().advance(maze.getValue()));
            if (interactiveGeneration.isSelected()) {
                view.refresh();
            }
            runGenerator();
        });
    }

    private void runVisitor() {
        if (!running.get()) {
            view.refresh();
            return;
        }
        if (visitor.getValue().isEnded()) {
            running.set(false);
            updateVisitor();
            view.refresh();
            return;
        }
        Platform.runLater(() -> {
            maze.setValue(visitor.getValue().advance(maze.getValue()));
            if (interactiveVisit.isSelected()) {
                view.refresh();
            }
            runVisitor();
        });
    }

    private void setCellValue(MouseEvent event) {
        if (running.get()) {
            return;
        }
        final int row = (int) (event.getY() / sizeRate.getValue());
        final int column = (int) (event.getX() / sizeRate.getValue());
        if (event.isPrimaryButtonDown()) {
            maze.getValue().apply(location -> location.row() == row && location.column() == column ? Maze.AISLE : location.value());
        }
        if (event.isSecondaryButtonDown()) {
            maze.getValue().apply(location -> location.row() == row && location.column() == column ? Maze.WALL : location.value());
        }
        view.refresh();
    }

    private void showLoadDialog(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load maze");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        final File target = fileChooser.showOpenDialog(this.getScene().getWindow());
        if (target != null) {
            maze.setValue(Maze.load(target.toPath()));
        }
    }

    private void showSaveDialog(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save maze");
        fileChooser.setInitialFileName("maze.png");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        final File target = fileChooser.showSaveDialog(this.getScene().getWindow());
        if (target != null) {
            Maze.persist(maze.getValue(), target.toPath());
        }
    }

    private static final Map<String, Supplier<MazeGenerator>> GENERATION_STRATEGIES = new LinkedHashMap<>();
    private static final Map<String, Supplier<MazeVisitor>> VISITORS = new LinkedHashMap<>();

    static {
        {
            GENERATION_STRATEGIES.put("DFS", () -> new DepthFirstGenerator(0, 0, new Random(System.nanoTime())));
            GENERATION_STRATEGIES.put("Kruskal", () -> new KruskalGenerator(new Random(System.nanoTime())));
            GENERATION_STRATEGIES.put("Prim", () -> new PrimGenerator(0, 0, new Random(System.nanoTime())));
            GENERATION_STRATEGIES.put("234/12345", () -> new LifeStrategy<>(i -> i >= 2 && i <= 4, i -> i >= 1 && i <= 5));
            GENERATION_STRATEGIES.put("23/12345", () -> new LifeStrategy<>(i -> i >= 2 && i <= 3, i -> i >= 1 && i <= 5));
            GENERATION_STRATEGIES.put("3/23", () -> new LifeStrategy<>(i -> i == 3, i -> i == 2 || i == 3));
            GENERATION_STRATEGIES.put("3/1234", () -> new LifeStrategy<>(i -> i == 3, i -> i >= 1 && i <= 4));
            GENERATION_STRATEGIES.put("3/12345", () -> new LifeStrategy<>(i -> i == 3, i -> i >= 1 && i <= 5));

            VISITORS.put("BFS", () -> new BFS(0, 0));
        }
    }
}
