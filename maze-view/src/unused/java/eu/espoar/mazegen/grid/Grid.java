package eu.espoar.mazegen.grid;

import java.util.List;

import eu.espoar.mazegen.Mapper;

public interface Grid<E> {

    int rows();

    int columns();

    Grid<E> fill(E element);

    /**
     * Apply mapper to the maze
     * 
     * @param mapper
     * @return
     */
    Grid<E> apply(Mapper<E> mapper);

    Location<E> at(int row, int column);

    List<Location<E>> locations();

    interface Location<E> {

        int row();

        int column();

        E value();

        Grid<E> set(E element);

        Location<E> moveBy(int rows, int columns);

        Grid<E> span(int rows, int columns);
    }
}
