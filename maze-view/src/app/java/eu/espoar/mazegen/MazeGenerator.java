package eu.espoar.mazegen;

import java.util.Random;

import eu.espoar.mazegen.generation.DepthFirstMazeGenerator;

/**
 * @author tono
 */
public class MazeGenerator {

	public static void main(String[] args) {
		DepthFirstMazeGenerator generator = new DepthFirstMazeGenerator(new Random(System.nanoTime()), 0.25d);
		Maze maze = generator.createMaze(21, 21, 3, 3);
		System.out.println(maze);
	}
}
