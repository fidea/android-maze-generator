package eu.espoar.mazeview;

import android.graphics.Canvas;

import eu.espoar.mazeview.mazegen.Maze;

/**
 * Interface for maze rendering engines
 *
 * Created by tono on 25.4.2016.
 */
public interface MazeRenderer {
    /**
     * draw given maze to canvas
     *
     * @param canvas canvas to use for drawing
     * @param maze maze to draw
     */
    void drawMaze(Canvas canvas, Maze maze);
}
