package eu.espoar.mazeview.rendering;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import eu.espoar.mazeview.MazeRenderer;
import eu.espoar.mazeview.mazegen.Maze;

/**
 * Simple maze renderer that fills canvas by the maze as much as possible
 *
 * Created by tono on 25.4.2016.
 */
public class BasicMazeRenderer implements MazeRenderer {
    private static final String TAG = "BasicMazeRenderer";
    private Paint paintMazeWall;

    public BasicMazeRenderer() {
        // init the paint
        paintMazeWall = new Paint();
        paintMazeWall.setColor(Color.rgb(50, 50, 50));
    }

    @Override
    public void drawMaze(Canvas canvas, Maze maze) {
        // get size of the view
        int height = canvas.getHeight();
        int width = canvas.getWidth();
        // get the size of the maze
        int rows = maze.rows();
        int cols = maze.columns();
        // get the size of the cell (square)
        float cellSize = Math.min(1f*height/rows, 1f*width/cols);
        float wallThickness = cellSize / 3;
        paintMazeWall.setStrokeWidth(wallThickness);
        Log.i(TAG, "Cell size is "+cellSize);
        for (int row=0; row<rows; row++) {
            for (int col=0; col<cols; col++) {
                drawCell(canvas, cellSize, wallThickness, maze.at(row, col),
                        maze.at(row-1, col), maze.at(row, col+1), maze.at(row+1, col), maze.at(row, col-1));
                canvas.translate(cellSize, 0f);
            }
            // move back to the beginning of row and a line lower
            canvas.translate(-(cellSize*cols), cellSize);
        }
    }

    private void drawCell(Canvas canvas, float cellSize, float wallThickness, byte status, byte statusUp, byte statusRight, byte statusDown, byte statusLeft) {
        float center = cellSize/2;
        // if this cell is a wall then draw it
        if (status == Maze.WALL) {
            canvas.drawCircle(center, center, wallThickness/2, paintMazeWall);
            drawConnector(canvas, center, 0, 0);
            // now draw also neighbors as those will want to be connected to this wall
            if (statusUp == Maze.WALL) {
                drawConnector(canvas, center, -center, 0);
            }
            if (statusRight == Maze.WALL) {
                drawConnector(canvas, center, 0, center);
            }
            if (statusDown == Maze.WALL) {
                drawConnector(canvas, center, center, 0);
            }
            if (statusLeft == Maze.WALL) {
                drawConnector(canvas, center, 0, -center);
            }
        }
    }

    private void drawConnector(Canvas canvas, float center, float offsetX, float offsetY) {
        canvas.drawLine(center, center, center+offsetY, center+offsetX, paintMazeWall);
    }

}
