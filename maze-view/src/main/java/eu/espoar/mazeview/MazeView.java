package eu.espoar.mazeview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import java.util.Random;

import eu.espoar.mazeview.mazegen.Maze;
import eu.espoar.mazeview.mazegen.generation.DepthFirstMazeGenerator;
import eu.espoar.mazeview.rendering.BasicMazeRenderer;

/**
 * main view displaying the maze and providing user interactions
 *
 * Created by tono on 23.4.2016.
 */
public class MazeView extends View {
    private static final String TAG = "MazeView";

    private Maze maze;
    private MazeRenderer mazeRenderer;

    public MazeView(Context context) {
        super(context);
        initialize();
    }

    public MazeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public MazeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    private void initialize() {
        setMazeRenderer(new BasicMazeRenderer());
        // if we are in designer come up with some maze
        if (isInEditMode()) {
            generateMaze(10, 10, 0.5d);
        }
    }


    public Maze getMaze() {
        return maze;
    }

    public void setMaze(Maze maze) {
        this.maze = maze;
        invalidate();
    }

    /**
     * Randomly generate maze with specified parameters. Maze is immediately displayed
     * by the MazeView instance
     *
     * @param rows number of maze rows
     * @param columns number of maze columns
     * @param difficulty difficulty of the maze in range (0,1)
     * @return the maze that was generated
     */
    public Maze generateMaze(int rows, int columns, double difficulty) {
        DepthFirstMazeGenerator dfsGen = new DepthFirstMazeGenerator(new Random(System.nanoTime()), difficulty);
        Maze maze = dfsGen.createMaze(rows*2+1, columns*2+1, 1, 1);
        setMaze(maze);
        return maze;
    }

    /**
     * Use renderer to draw the maze
     *
     * @param canvas the canvas on which the background will be drawn
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the maze
        if (maze != null) {
            mazeRenderer.drawMaze(canvas, maze);
        }
    }

    /**
     * set the maze renderer for the view
     *
     * @param mazeRenderer new maze renderer to use
     */
    public void setMazeRenderer(MazeRenderer mazeRenderer) {
        this.mazeRenderer = mazeRenderer;
    }
}
