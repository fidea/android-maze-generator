package eu.espoar.mazeview.mazegen.generation;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import eu.espoar.mazeview.mazegen.Maze;

/**
 * Maze generator built around depth first search with somewhat random direction
 * 
 * NOTE that this implementation is not thread safe
 * 
 * @author tono
 */
public class DepthFirstMazeGenerator {
	/** random used to decide on the direction */
    private final Random random;
    /** chances that the direction will change */
    private final double directionStability;
    
	private final Deque<Position> path = new LinkedList<>();
    private Direction currentDirection;
    
    /**
     * Create new generator with specified direction stability
     * 
     * @param random random used to pick next direction
     * @param directionInStability chances that the direction will change. Recommended .25
     */
    public DepthFirstMazeGenerator(Random random, double directionInStability) {
        // check and store reference to random
        if (random == null) {
        	throw new IllegalArgumentException("Random can not be null");
        }
        this.random = random;
        // check and note down direction stability
        if (directionInStability<=0 || directionInStability>=1) {
        	throw new IllegalArgumentException("Direction stability needs to be in range (0, 1) but was "+directionInStability);
        }
        this.directionStability = directionInStability;
    }

    /**
     * create new maze with specified size and star from initial position
     * 
     * @param rows number of rows
     * @param columns number of columns
     * @param initialRow row to star from
     * @param initialColumn col to start from
     * @return the new maze generated based on the specs
     */
    public Maze createMaze(int rows, int columns, int initialRow, int initialColumn) {
    	// create new maze of specified size
    	Maze maze = new Maze(rows, columns);
    	// check and put initial position to the path
    	if (initialRow<0 || initialColumn<0) {
        	throw new IllegalArgumentException("Initial position can not have negative coordinates but was "+initialRow+"x"+initialColumn);
    	}
        path.push(new Position(initialRow, initialColumn));
    	// create maze
    	while (!path.isEmpty()) {
    		advance(maze);
    	}
    	// return the maze
    	return maze;
    }
    
    /**
     * Advance the maze creation process
     * 
     * @param maze maze to apply the generator to
     */
    private void advance(Maze maze) {
    	// if path is empty then maze is considered done
        if (path.isEmpty()) {
            return;
        }
        // set first item on the path as aisle
        final Position position = path.peek();
        maze.setAisle(position);
        // get list of possible directions
        final List<Direction> neighbours = getAvailableDirections(maze, position);
        // if there is no place to move remove current path item and recursively move on
        if (neighbours.isEmpty()) {
            path.remove();
            advance(maze);
            return;
        }
        // pick direction to follow
        chooseDirection(neighbours);
        // set aisle at the picked direction
        maze.setAisle(position.move(currentDirection, 1));
        // add new element to the path a step ahead of currently marked position
        path.push(position.move(currentDirection, 2));
    }

    /**
     * get list of directions which are available for next move. Algorithm avoids cells where it
     * already had been
     * 
     * @param maze maze to check
     * @param position position to check
     * @return list of directions in which generator can move
     */
    private static List<Direction> getAvailableDirections(Maze maze, Position position) {
        final List<Direction> directions = new ArrayList<>(4);
        // direction is valid if wall is there (i.e. we have not visited that place yet)
        if (isWall(maze, position, Direction.UP, 2)) {
            directions.add(Direction.UP);
        }
        if (isWall(maze, position, Direction.DOWN, 2)) {
            directions.add(Direction.DOWN);
        }
        if (isWall(maze, position, Direction.LEFT, 2)) {
            directions.add(Direction.LEFT);
        }
        if (isWall(maze, position, Direction.RIGHT, 2)) {
            directions.add(Direction.RIGHT);
        }
        return directions;
    }

    /**
     * return true if there is wall when one moves from specified position
     * 
     * @param maze maze to move in
     * @param position starting position
     * @param direction direction of the movement
     * @param by number of steps to take
     * 
     * @return true is wall was found, false otherwise
     */
    private static boolean isWall(Maze maze, Position position, Direction direction, int by) {
    	// return true if maze has in specified position wall
    	return maze.at(position.move(direction, by)) == Maze.WALL;
    }
    
    /**
     * choose direction to move in
     * 
     * @param availableDirections list of available directions
     */
    private void chooseDirection(List<Direction> availableDirections) {
    	// change direction with specified probability
        if (!availableDirections.contains(currentDirection) || random.nextDouble() < directionStability) {
        	// pick random direction from available directions
            currentDirection = availableDirections.get(random.nextInt(availableDirections.size()));
        }
    }
}
