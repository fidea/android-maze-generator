package eu.espoar.mazeview.mazegen.generation;

/**
 * direction specified by offsets from current position
 * 
 * @author tono
 */
public enum Direction {
    UP(-1, 0),
    DOWN(1, 0),
    LEFT(0, -1),
    RIGHT(0, 1);

    private final int rowOffset;
    private final int columnOffset;

    /**
     * create new direction instance representing specified offset
     * 
     * @param rowOffset
     * @param columnOffset
     */
    private Direction(int rowOffset, int columnOffset) {
        this.rowOffset = rowOffset;
        this.columnOffset = columnOffset;
    }
    
    /**
     * move from old position to new one in this direction by specified number of steps
     * 
     * @param oldPosition position which we are moving away from
     * @param count number of steps to take
     * @return new position
     */
    public Position applyTo(Position oldPosition, int count) {
    	return new Position(oldPosition.row + rowOffset*count, oldPosition.column+columnOffset*count);
    }

	/**
	 * @return the rowOffset
	 */
	public int getRowOffset() {
		return rowOffset;
	}

	/**
	 * @return the columnOffset
	 */
	public int getColumnOffset() {
		return columnOffset;
	}
    
    
}
