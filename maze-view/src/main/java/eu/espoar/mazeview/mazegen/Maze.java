/**
 * 
 */
package eu.espoar.mazeview.mazegen;

import eu.espoar.mazeview.mazegen.generation.Position;

/**
 * @author tono
 */
public class Maze {
	public static final byte WALL = 0;
	public static final byte AISLE = 1;
	public static final byte OUTSIDE = 2;
	
    private byte[][] maze;

	/**
	 * create new all-walls maze. Minimum size is 3x3
	 *
	 * @param rows number of rows
	 * @param columns number of columns
     */
	public Maze(int rows, int columns) {
		// check arguments
		if (rows < 3 || columns < 3) {
			throw new IllegalArgumentException(rows+"x"+columns+" is not valid maze size. Has to be 3x3 or bigger");
		}
		this.maze = new byte[rows][columns];
	}

	/**
	 * get value at specified position (AISLE or WALL) or get OUTSIDE when coordinates are outside of the maze
	 * 
	 * @param position position to check
	 * @return the value at the position (AISLE, WALL, OUTSIDE)
	 */
	public byte at(Position position) {
    	return at(position.row, position.column);
    }	
	
	/**
	 * get value at specified position (AISLE or WALL) or get OUTSIDE when coordinates are outside of the maze
	 * 
	 * @param row row to check
	 * @param column column to check
	 * @return the value at the position (AISLE, WALL, OUTSIDE)
	 */
	public byte at(int row, int column) {
		// check whether row is within the maze
		if (row < 0 || row >= rows()) {
			return OUTSIDE;
		}
		// check whether column is within the maze
		if (column < 0 || column >= columns()) {
			return OUTSIDE;
		}
    	return maze[row][column];
    }
	
	/**
	 * set AISLE at specified position
	 * 
	 * @param position position to update
	 */
	public void setAisle(Position position) {
    	maze[position.row][position.column] = AISLE;
    }

	/**
	 * number of rows in the maze
	 * 
	 * @return number of rows
	 */
	public int rows() {
		return maze.length;
	}

	/**
	 * number of columns in the maze
	 * 
	 * @return number of columns
	 */
	public int columns() {
		return maze[0].length;
	}
	
	@Override
	public String toString() {
		int rows = rows();
		int columns = columns();
		StringBuilder sb = new StringBuilder("Maze["+rows+"x"+columns+"]\n");
		// now the maze
		for (int row=0; row<rows; row++) {
			for (int col=0; col<columns; col++) {
				sb.append(maze[row][col]==WALL?"X":" ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
