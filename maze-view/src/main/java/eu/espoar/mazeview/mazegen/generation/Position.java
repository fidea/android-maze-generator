/**
 * 
 */
package eu.espoar.mazeview.mazegen.generation;

/**
 * position defined as row and column
 * 
 * @author tono
 */
public class Position {
	public final int row;
	public final int column;
	
	/**
	 * create new position on specified row and column
	 *
	 * @param row desired row
	 * @param column desired column
	 */
	public Position(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	/**
	 * move from current position to new position
	 * 
	 * @param direction direction of movement
	 * @param steps number of steps to take
	 * 
	 * @return new position
	 */
	public Position move(Direction direction, int steps) {
		return direction.applyTo(this, steps);
	}
}
